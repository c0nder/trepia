<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answers extends Model
{
    protected $table = 'answers';

    public $timestamps = false;

    protected $fillable = [
        'id', 'quest_id', 'bloger_id', 'text', 'time'
    ];

    public function question() {
        return $this->belongsTo('App\Questions');
    }
}
