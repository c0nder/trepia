<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Questions
 *
 * @property-read \App\User $user
 * @mixin \Eloquent
 */
class Questions extends Model
{
    protected $table = 'questions';

    protected $fillable = [
        'id', 'uid', 'bloger_id', 'text', 'time', 'answered'
    ];

    public function users() {
        return $this->belongsTo('App\User');
    }

    public function bloger() {
        return $this->hasMany('App\User', 'id', 'bloger_id');
    }

    public function user() {
        return $this->hasMany('App\User', 'id', 'uid');
    }

    public function answer() {
        return $this->hasOne('App\Answers', 'quest_id', 'id');
    }
}
