<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Redirect;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!User::isAdmin()) {
            return Redirect::to('/');
        }

        return $next($request);
    }
}
