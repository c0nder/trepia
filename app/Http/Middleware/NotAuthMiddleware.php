<?php

namespace App\Http\Middleware;

use App\User;
use Auth;
use Closure;
use Redirect;

class NotAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) {
            return Redirect::to('dashboard');
        }

        return $next($request);
    }
}
