<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'UsersController@mainPage');

//Youtuber auth

Route::get('auth/ytb/reg', function() {
	return view('auth.ytb_reg');
});

//user registration

Route::group(['middleware' => 'notauth'], function() {
    Route::get('auth/user/reg', 'UsersController@getRegister');

    Route::post('auth/user/reg', 'UsersController@postRegister');
});

//user admin

Route::group(['middleware' => 'admin'], function() {
    Route::get('admin', 'AdminController@showAdminMenu');
    Route::post('admin/add/user', 'AdminController@postRegister');
    Route::get('admin/loginUser/{id}', 'AdminController@loginAsUser');
});

//user profile

Route::group(['middleware' => ['auth']], function() {
    Route::get('dashboard', 'UserProfileController@myProfile');

    Route::get('dashboard/logout', 'UserProfileController@Logout');

    Route::post('dashboard/post', 'UserWallController@post');

    Route::get('dashboard/questions', 'UserProfileController@showQuestions');

    Route::post('dashboard/questions', 'AskController@answerToQuestion');

    Route::get('dashboard/settings', 'UserProfileController@showSettings');

    Route::post('dashboard/settings/save', 'UserProfileController@saveSettings');

    Route::post('dashboard/bloger/settings/save', 'YoutuberProfileController@saveSettings');

    Route::get('dashboard/delete/post/{id}', 'UserWallController@delete');

    //user payment

    Route::post('dashboard/payment', 'UserProfileController@addBalance');

    //user subscribe

    Route::get('subscribe/{id}', 'SubscribeController@validation');

    //user ask ytb

    Route::get('ask/{id}', 'AskController@showAskForm');
    Route::post('ask/{id}', 'AskController@postQuestion');

    //payment success and fails

    Route::get('payment/success', 'UserProfileController@paymentSuccess');
});

Route::get('dashboard/{id}', 'UserProfileController@showProfile');

//auth user

Route::post('user/register/auth', 'UsersController@AuthUser');

Route::get('payment/result', 'UserProfileController@paymentResult');