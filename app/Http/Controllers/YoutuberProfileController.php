<?php

namespace App\Http\Controllers;

use App\Subscription;
use App\User;
use App\Wall;
use Auth;
use Hash;
use Illuminate\Http\Request;

use App\Http\Requests;
use Input;
use Redirect;
use Validator;

class YoutuberProfileController extends Controller
{
    public function myProfile() {
        $user = Auth::user();
        $posts = Wall::getPosts($user->id);

        return view('user.youtuber.wall', compact('user', 'posts'));
    }

    public function showSettings() {
        $user = Auth::user();

        return view('user.youtuber.settings', compact('user'));
    }

    public function saveSettings() {
        $user = Auth::user();

        $rules = [
            'users_email' => 'email'
        ];

        if(Input::get('old_password')) {
            $rules = array_merge($rules, [
                'old_password' => 'min:6|max:20',
                'new_password' => 'min:6|max:20'
            ]);
        }

        if(Input::get('users_email') != $user->users_email) {
            $rules = array_merge($rules, [
                'users_email' => 'unique:users'
            ]);
        }

        $input = Input::all();
        $validator = Validator::make($input, $rules);

        $validator->passes();

        if (Input::get('old_password') && !Hash::check(Input::get('old_password'), $user->password)) {
            $validator->getMessageBag()->add('old_password', 'Неверный старый пароль');
        }

        if (count($validator->getMessageBag()->all()) === 0) {
            $user->setAttribute('users_email', $input['users_email']);
            $user->setAttribute('sub_price', $input['sub_price']);

            if (Input::get('new_password')) {
                $user->setAttribute('password', Hash::make($input['new_password']));
            }

            $user->save();

            return Redirect::to('dashboard/settings')
                ->with('success', true);
        }

        return Redirect::to('dashboard/settings')->withErrors($validator, 'update_info');

    }

    public function showProfile($pid) {
        $posts = Wall::getPosts($pid);

        $user = $this->guestUserProfile($pid);

        if(!empty($user) && $user->users_type == 2) {
            return view('user.youtuber.guest.wall', compact('user', 'posts'));
        } else {
            return view('user/guest/user_not_found');
        }
    }

    public function postQuestion() {

    }

    public function logout() {
        Auth::logout();

        return Redirect::to('/');
    }

    private function guestUserProfile($uid)
    {
        $user = User::userById($uid);
        return $user;
    }
}
