<?php

namespace App\Http\Controllers;


use App\Http\Requests;


use App\Subscription;
use Carbon\Carbon;
use DB;

use Hash;
use Input;
use Redirect;
use Request;
use Validator;
use View;

use App\User;

use Auth;

class UsersController extends Controller
{
    public function mainPage() {
        $youtubers = User::where('users_type', '=', 2)->orderBy('subscribers', 'desc')->get();

        return view('Index', compact('youtubers'));
    }

	public function getRegister() {
		return View::make('auth.register');
	}

	public function postRegister(Request $request)
	{
		$validator = Validator::make(Input::all(), [
	        'users_name' => 'min:6|max:25',
	        'users_email' => 'email|unique:users',
	        'users_pass' => 'min:6|max:20|same:users_confirm_pass',
	        'users_confirm_pass' => 'min:6|max:20',
	        'users_inf' => 'min:20|max:200'
		]);

		if($validator->fails()) {
			return Redirect::back()->withErrors($validator, 'register');
		}

		// After the validation we have to add our user to our Database

		$user = User::create([
            'users_email' => Input::get('users_email'),
			'users_name' => Input::get('users_name'),
			'password' => Hash::make(Input::get('users_pass')),
			'users_inf' => Input::get('users_inf'),
            'balance' => 0,
            'subscribers' => 0,
            'users_type' => 0,
            'avatar' => 'https://www.staffsprep.com/software/flat_faces_icons/png/flat_faces_icons_circle/flat-faces-icons-circle-3.png',
            'videos' => 0,
            'views' => 0,
            'sub_price' => 0,
            'youtube_chanel' => NULL
		]);

		// After adding the user to our Database i decided to authenticate our user by id

		if(Auth::loginUsingId($user->id)) {
			return Redirect::to('dashboard');
		} else {
			return Redirect::back();
		}
	}

	public function AuthUser(Request $request) {
		$validator = Validator::make(Input::all(), [
		    'users_email' => 'required',
            'users_pass' => 'required|min:6|max:20'
		]);

		if($validator->fails()) {
			return Redirect::back()->withErrors($validator, "login");
		}

		$login = Input::get('users_email');
		$password = Input::get('users_pass');

		if (Auth::attempt(['users_email' => $login, 'password' => $password], 1)) {
    		return Redirect::to('dashboard');
		} else {
            $validator->errors()->add('Auth', 'Логин или пароль введены не верно');

            return Redirect::back()->withErrors($validator, "login");
		}
	}
}