<?php

namespace App\Http\Controllers;

use Alaouy\Youtube\Youtube;
use App\User;
use Auth;
use Hash;
use Illuminate\Http\Request;

use App\Http\Requests;
use Input;
use Redirect;
use Validator;

class AdminController extends Controller
{
    public function  __construct()
    {

    }

    public function showAdminMenu() {
        $user = Auth::user();
        $users = User::all();
        return view('admin/Main', compact('user', 'users'));
    }

    public function postRegister() {
        $input = Input::all();
        $youtube = new Youtube('AIzaSyAUMdlQMS2foV9HFw1cpIj2YuW8utQ4oZk');

        $rules = [
            'users_email' => 'email|unique:users',
            'users_pass' => 'min:6|max:20|same:users_confirm_pass',
        ];

        $validator = Validator::make($input, $rules);

        $validator->passes();

        if (count($validator->getMessageBag()->all()) === 0) {
            $channel = $youtube->getChannelFromURL($input["ytb_url"]);

            $avatar = $channel->snippet->thumbnails->default->url;
            $username = $channel->snippet->title;
            $description = $channel->snippet->description;
            $subscribers = $channel->statistics->subscriberCount;
            $videos = $channel->statistics->videoCount;
            $views = $channel->statistics->viewCount;

            $user = User::create([
                'users_email' => $input['users_email'],
                'users_name' => $username,
                'password' => Hash::make($input['users_pass']),
                'users_inf' => $description,
                'balance' => 0,
                'subscribers' => $subscribers,
                'users_type' => 2,
                'avatar' => $avatar,
                'videos' => $videos,
                'views' => $views,
                'sub_price' => 150,
                'youtube_chanel' => $input['ytb_url']
            ]);

            return Redirect::to('admin')
                ->with('success', true);
        }

        return Redirect::back()
            ->withErrors($validator, 'register');

    }

    public function loginAsUser($id) {
        $user = Auth::loginUsingId($id);

        return Redirect::to('dashboard/'.$id);
    }
}
