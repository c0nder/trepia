<?php

namespace App\Http\Controllers;

use App\Subscription;
use App\User;
use Auth;
use Illuminate\Http\Request;

use App\Http\Requests;
use Redirect;

class SubscribeController extends Controller
{
    public function validation($uid) {
        $auth_user = Auth::user();
        $sub_user = User::userById($uid);

        $success = false;
        $message = "";

        if(!empty($sub_user) && $sub_user->users_type == 2) {
            if($auth_user->balance >= $sub_user->sub_price) {
                if($this->subscribeUser($sub_user)) {
                    $success = true;

                    $message = "Ваша подписка на блогера " . $sub_user->users_name . " на месяц успешно оформлена.";
                    return Redirect::to('dashboard/'.$sub_user->id)->with('header_success_message', $message);
                } else {
                    $message = "Что-то пошло не так. Повторите попытку позже.";
                }
            } else {
                $message = "У вас не достаточно средств для подписки на данного блогера.";
            }
        }

        if(!$success) {
            return Redirect::back()->with('header_error_message', $message);
        }
    }

    public function subscribeUser($sub_user) {
        $subModel = new Subscription();

        if($subModel->subscribeUser($sub_user)) {
            return true;
        } else {
            return false;
        }
    }
}
