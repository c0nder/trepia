<?php

namespace App\Http\Controllers;

use App\Answers;
use App\Payments;
use App\Questions;
use App\Subscription;
use DB;
use Hash;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

use Auth;
use Input;
use Redirect;
use Validator;
use App\Wall;

class UserProfileController extends Controller
{
	function __construct() {
		$this->user = Auth::user();
		$this->uid = Auth::id();
	}

    public function showProfile($pid) {
        $posts = Wall::getPosts($pid);
        $user = User::find($pid);
        $template = 'user.guest.dashboard';
        $include_temp = 'user.guest.wall';

        if(!empty($user)) {
            if($user->id == $this->uid) {
                return Redirect::to('dashboard');
            }

            if($user->users_type == 2) {
                $isSigned = Subscription::isSigned($user->id);
                $template = 'user.guest.youtuber.dashboard';
            }

            return view($template, compact('user', 'posts', 'isSigned', 'include_temp'));
        } else {
            return view('user/guest/user_not_found');
        }
    }

    public function myProfile() {
        $user = Auth::user();
        $posts = Wall::getPosts($user->id);
        $template = '';

        if(User::isYoutuber()) {
            $template = 'user.youtuber.dashboard';
        } else {
            $template = 'user.dashboard';
        }

        return view('user.wall', compact('user', 'posts', 'template'));
    }

    public function showSettings() {
        $user = Auth::user();
        $posts = Wall::getPosts($user->id);

        if(User::isYoutuber()) {
            $template = 'user.settings.youtuber.settings';
        } else {
            $template = 'user.settings.settings';
        }

        return view($template, compact('user','posts'));
    }

    public function saveSettings() {
        $user = Auth::user();

        $rules = [
            'users_name' => 'min:6|max:25',
            'users_email' => 'email',
            'users_inf' => 'min:20|max:200'
        ];

        if(Input::get('old_password')) {
            $rules = array_merge($rules, [
                'old_password' => 'min:6|max:20',
                'new_password' => 'min:6|max:20'
            ]);
        }

        if(Input::get('users_email') != $user->users_email) {
            $rules = array_merge($rules, [
                'users_email' => 'unique:users'
            ]);
        }

        $input = Input::all();
        $validator = Validator::make($input, $rules);

        $validator->passes();

        if (Input::get('old_password') && !Hash::check(Input::get('old_password'), $user->password)) {
            $validator->getMessageBag()->add('old_password', 'Неверный старый пароль');
        }

        if (count($validator->getMessageBag()->all()) === 0) {
            $user->setAttribute('users_name', $input['users_name']);
            $user->setAttribute('users_email', $input['users_email']);
            $user->setAttribute('users_inf', $input['users_inf']);

            if (Input::get('new_password')) {
                $user->setAttribute('password', Hash::make($input['new_password']));
            }

            $user->save();

            return Redirect::to('dashboard/settings')
                ->with('success', true);
        }

        return Redirect::to('dashboard/settings')->withErrors($validator, 'update_info');

    }

	public function Logout() {
		Auth::logout();

		return Redirect::to('auth/user/reg');
	}

	public function showQuestions() {
	    $user = Auth::user();
        $posts = User::find(Auth::id())->posts;
        $template = 'user.questions';

        if($user->users_type == 2) {
            $questions = User::find(Auth::id())->questionsBloger->sortByDesc('id');

            $template = 'user.youtuber.questions';
        } else {
            $questions = User::find(Auth::id())->questions->sortByDesc('id');
        }

        return view($template, compact('user', 'questions', 'posts'));
    }

    public function addBalance() {
        $payment = new \Idma\Robokassa\Payment(
            'trpia', 'p8HhJJ36I4nlv8hnNlEF', 'j5FbHp6G05cW5GdQvVqZ', true
        );

        $sum = Input::get('sum');

        if(empty($sum) || $sum === 0) {
            return Redirect::back()->with('header_error_message', 'Сумма баланса не должна равняться 0');
        }

        $order = Payments::create([
            'uid' => Auth::id(),
            'sum' => $sum,
            'success' => 0
        ]);

        if($order) {
            $payment = $payment
                ->setInvoiceId($order->id)
                ->setSum($sum)
                ->setDescription('Пополнение баланса личного кабинета.');

            if($payment) {
                return Redirect::to($payment->getPaymentUrl());
            } else {
                return Redirect::back()->with('header_error_message', 'Произошла ошибка. Повторите попытку позже или напишите в службу поддержки');
            }
        }
    }

    public function paymentSuccess() {
        $payment = new \Idma\Robokassa\Payment(
            'trpia', 'p8HhJJ36I4nlv8hnNlEF', 'j5FbHp6G05cW5GdQvVqZ', true
        );

        if ($payment->validateSuccess($_GET)) {
            $order = Payments::find($payment->getInvoiceId());

            if ($payment->getSum() == $order->sum && $order->uid == Auth::id()) {
                return Redirect::to('dashboard')->with('header_success_message', 'Вы успешно пополнили баланc на $order->sum рублей');
            } else {
                return Redirect::to('dashboard');
            }
        } else {
            return Redirect::to('dashboard');
        }
    }

    public function paymentResult() {
        $payment = new \Idma\Robokassa\Payment(
            'trpia', 'p8HhJJ36I4nlv8hnNlEF', 'j5FbHp6G05cW5GdQvVqZ', true
        );

        if ($payment->validateResult($_GET)) {
            $order = Payments::find($payment->getInvoiceId());

            if ($payment->getSum() == $order->sum && $order->success != 1) {
                $user = User::find($order->uid);

                $order->success = 1;
                $order->save();

                $user->balance += $order->sum;
                $user->save();
            }

            // send answer
            echo $payment->getSuccessAnswer(); // "OK1254487\n"
        }
    }
}