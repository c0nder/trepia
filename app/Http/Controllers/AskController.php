<?php

namespace App\Http\Controllers;

use App\Answers;
use App\Subscription;
use App\User;
use App\Wall;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use Input;
use Redirect;

class AskController extends Controller
{
    public function validation($bid) {
        if (Subscription::isSigned($bid)) {
            return true;
        } else {
            return false;
        }
    }

    public function showAskForm($bid) {
        if($this->validation($bid)) {
            $user = User::userById($bid);
            $template = 'user.guest.youtuber.dashboard';
            $include_temp = 'ask.ask';
            $isSigned = true;
            $posts = Wall::getPosts($bid);

            return view($template, compact('user', 'isSigned', 'include_temp', 'posts'));
        } else {
            return Redirect::to('dashboard/'.$bid)->with('header_error_message', 'Вы не подписаны на данного блогера');
        }
    }

    public function postQuestion($bid) {
        $user = Auth::user();

        $last_quest = DB::table('questions')
            ->where('uid', '=', $user->id)
            ->where('bloger_id', '=', $bid)
            ->orderBy('id', 'desc')
            ->first();

        if(!empty($last_quest) && $last_quest->answered == 0) {
            return Redirect::back()
                ->with('header_error_message', 'Вы не можете задать следующий вопрос, пока вам не ответили на предыдущий.');
        } else {
            $query = DB::table('questions')
                ->insert([
                    'id' => NULL,
                    'uid' => $user->id,
                    'bloger_id' => $bid,
                    'text' => Input::get('question'),
                    'time' => Carbon::now(),
                    'answered' => 0
                ]);

            if($query) {
                return Redirect::back()
                    ->with('header_success_message', 'Вопрос был успешно задан. Ответ отобразиться у вас в личном кабинете.');
            }
        }
    }

    public function answerToQuestion() {
        $user = Auth::user();
        $input = Input::all();

        if(User::isYoutuber() && !empty($input)) {
            $answer = new Answers();

            $query = $answer::create([
                'quest_id' => $input['question_id'],
                'bloger_id' => $user->id,
                'text' => $input['text'],
                'time' => Carbon::now()
            ]);

            if($query) {
                return Redirect::refresh();
            }
        } else {
            return Redirect::back();
        }
    }
}
