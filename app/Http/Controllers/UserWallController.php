<?php

namespace App\Http\Controllers;

use App\Posts;
use App\User;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use Input;
use Redirect;
use App\Wall;

class UserWallController extends Controller
{
    public function __construct() {
        $this->user = Auth::user();
        $this->id = Auth::id();
    }

    public function post() {
        Wall::post();

        return Redirect::back();
    }

    public function delete($id) {
        Wall::deletePost($id);
        return Redirect::to('dashboard');
    }
}
