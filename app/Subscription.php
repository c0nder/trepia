<?php

namespace App;

use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;
use Redirect;

/**
 * App\Subscription
 *
 * @mixin \Eloquent
 */
class Subscription extends Model
{
    protected $fillable = [
        'sub_id', 'bloger_id', 'created_at','lasts_at'
    ];

    public function subscribeUser($bloger) {
        $user = Auth::user();

        if(!$this->isSigned($bloger->id)) {
            $user->setAttribute('balance', $user->balance - $bloger->sub_price);
            $user->save();

            $sum_bloger = ($bloger->sub_price * 80) / 100;
            $update_bloger_balance = User::find($bloger->id);
            $update_bloger_balance->balance += $sum_bloger;
            $update_bloger_balance->save();

            $sum_admin = $bloger->sub_price - $sum_bloger;
            $update_admin_balance = User::find(1);
            $update_admin_balance->balance += $sum_admin;
            $update_admin_balance->save();

            return DB::table('subscriptions')->insert([
                'sub_id' => Auth::id(),
                'bloger_id' => $bloger->id,
                'created_at' => Carbon::now(),
                'lasts_at' => Carbon::now()->addMonths(1)
            ]);
        } else {
            return Redirect::to('dashboard/'.$bloger->id)
                ->with('header_error_message', 'Вы уже подписаны на этого блогера');
        }
    }

    public static function isSigned($uid) {
        $user = Auth::user();

        if(!empty($user)) {
            $query = DB::table('subscriptions')
                ->where('sub_id', '=', $user->id)
                ->where('bloger_id', '=', $uid)
                ->count();

            if($query > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
