<?php

namespace App\Console\Commands;

use Alaouy\Youtube\Youtube;
use App\User;
use Illuminate\Console\Command;

class Blogers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'blogers:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $youtube = new Youtube('AIzaSyAUMdlQMS2foV9HFw1cpIj2YuW8utQ4oZk');

        $blogers = User::where('users_type', '=', 2)->get();

        foreach ($blogers as $bloger) {
            if(!empty($bloger->youtube_chanel)) {
                $user = User::find($bloger->id);
                $channel = $youtube->getChannelFromURL($bloger->youtube_chanel);

                $avatar = $channel->snippet->thumbnails->default->url;
                $username = $channel->snippet->title;
                $description = $channel->snippet->description;
                $subscribers = $channel->statistics->subscriberCount;
                $videos = $channel->statistics->videoCount;
                $views = $channel->statistics->viewCount;

                $user->avatar = $avatar;
                $user->users_name = $username;
                $user->users_inf = $description;
                $user->subscribers = $subscribers;
                $user->videos = $videos;
                $user->views = $views;

                if($user->save()) {
                    $this->info('User: '.$username.' has been updated!');
                }
            }
        }
    }
}
