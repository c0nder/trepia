<?php

namespace App\Console\Commands;

use App\Subscription;
use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;

class Subscriptions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sub:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $subscriptions = Subscription::all();
        $date = Carbon::now()->toDateString();

        $sub_ids = [];

        foreach ($subscriptions as $sub) {
            if($date == $sub->lasts_at) {
                $sub_ids[] = $sub->id;
            }
        }

        $db = DB::table('subscriptions')
            ->whereIn('id', $sub_ids)
            ->delete();

        if($db) {
            $this->info('Subscriptions were deleted XD');
        } else {
            $this->info('Subscriptions were not deleted :(');
        }
    }
}
