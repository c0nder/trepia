<?php

namespace App;

use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;
use Input;
use Redirect;

/**
 * App\Wall
 *
 * @mixin \Eloquent
 */
class Wall extends Model
{
    protected $table = 'wall';

    protected $fillable = [
        'id', 'uid', 'message', 'created_at'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public static function post() {
        return DB::table('wall')->insert(
            ['id' => NULL, 'uid' => Auth::id(), 'message' => Input::get('post_text'), 'created_at' => Carbon::now()]
        );
    }

    public static function deletePost($id) {
        return DB::table('wall')
            ->where('uid', '=', Auth::id())
            ->where('id', '=', $id)
            ->delete();
    }

    public static function getPosts($id = null) {
        if(!is_null($id)) {
            return DB::table('wall')
                    ->where('uid', '=', $id)
                    ->orderBy('id', 'DESC')
                    ->get();
        } else {
            return DB::table('wall')
                ->orderBy('id', 'DESC')
                ->get();
        }
    }
}
