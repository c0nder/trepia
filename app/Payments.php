<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    protected $table = "payments";

    protected $fillable = [
        'id', 'uid', 'sum', 'success', 'created_at', 'updated_at'
    ];
}
