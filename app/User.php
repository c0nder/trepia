<?php

namespace App;

use Auth;
use DB;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Str;

use Illuminate\Support\Facades\Log;

use Input;
use Mail;

/**
 * App\User
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Questions[] $questions
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'users';

    protected $fillable = [
        'users_name', 'users_email', 'password', 'users_inf', 'balance', 'avatar', 'subscribers', 'users_type', 'videos', 'views', 'sub_price', 'youtube_chanel'
    ];

    public static $validation = [
        'users_name' => 'min:6|max:25',
        'users_email' => 'email|unique:users',
        'users_pass' => 'min:6|max:20|same:users_confirm_pass',
        'users_confirm_pass' => 'min:6|max:20',
        'users_inf' => 'min:20|max:200'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    public function questions() {
        return $this->hasMany('App\Questions', 'uid');
    }

    public function questionsBloger() {
        return $this->hasMany('App\Questions', 'bloger_id');
    }

    public function posts() {
        return $this->hasMany('App\Wall', 'uid');
    }

    public function question() {
        return $this->belongsTo('App\Questions');
    }

    public static function userById($uid) {
        return DB::table('users')->where('id', '=', $uid)->first();
    }

    public static function isYoutuber() {
        $user = Auth::user();

        if($user->users_type != 2) {
            return false;
        } else {
            return true;
        }
    }

    public static function isAdmin() {
        $user = Auth::user();

        if($user->users_type != 1) {
            return false;
        } else {
            return true;
        }
    }

    public static function updateUserBalance($uid = null, $sum) {
        if($uid == null) {
            $uid = Auth::user()->id;
        }

        return DB::table('users')
            ->where('id', '=', $uid)
            ->update(['balance' => $sum]);
    }

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }
}
