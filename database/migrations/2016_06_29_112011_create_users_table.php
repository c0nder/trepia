<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function(Blueprint $table) {
            // ID пользователя
            $table->increments('id');
 
            // E-Mail (уникальный)
            $table->string('users_email')->unique();
 
            // Пароль. Для используемой в Laravel хэш-функции требуется не меньше 60 символов
            $table->string('password', 60);

            $table->string('users_inf', 250);
 
            // Никнейм
            $table->string('users_name')->unique();
 
            // Админ?
            $table->integer('users_type')->length(11);
 
            // Токен для возможности запоминания пользователя
            $table->rememberToken(); // remember_token

            // created_at, updated_at
            $table->timestamps();

            $table->integer('balance')->length(11);

            $table->string('avatar');

            $table->integer('subscribers')->length(11);

            $table->integer('videos')->length(11);

            $table->integer('views')->length(11);

            $table->integer('sub_price')->length(11);

            $table->string('youtube_chanel', 255);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
