<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function(Blueprint $table) {
            // ID пользователя
            $table->increments('id');

            $table->integer('uid')->length(11);

            $table->integer('bloger_id')->length(11);

            $table->string('text', 1000);

            $table->dateTime('time');

            $table->integer('answered')->length(11);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
