@extends('Main')

@section('title')
    Регистрация пользователя | Trepia
@endsection

@section('content')
    <div class="page">
        <h1 class="page_title">Зарегистрируйтесь или войдите</h1>
        {{ Form::open(array('url' => 'user/register/auth', 'id' => 'ent_form')) }}
        @if(count($errors->getBag('login')->all()) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->getBag('login')->all() as $error)
                        <li>{{ $error  }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {{ Form::text('users_email', '', array('class' => 'form_text', 'placeholder' => 'E-mail', 'required' => 'required')) }}
        {{ Form::password('users_pass', array('class' => 'form_text', 'placeholder' => 'Пароль', 'required' => 'required')) }}

        {{ Form::submit('Войти на сайт', array('class' => 'form_btn')) }}
        {{ Form::close() }}

        {{ Form::open(array('id' => 'reg_form')) }}

        @if(count($errors->getBag('register')->all()) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->getBag('register')->all() as $error)
                        <li>{{ $error  }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {{ csrf_field() }}

        {{ Form::text('users_name', '', array('class' => 'form_text', 'placeholder' => 'Имя и фамилия', 'required' => 'required')) }}
        {{ Form::email('users_email', '', array('class' => 'form_text', 'placeholder' => 'Email', 'required' => 'required')) }}
        {{ Form::password('users_pass', array('class' => 'form_text', 'placeholder' => 'Пароль', 'required' => 'required')) }}
        {{ Form::password('users_confirm_pass', array('class' => 'form_text', 'placeholder' => 'Повторите пароль', 'required' => 'required')) }}
        {{ Form::textarea('users_inf', null, array('class' => 'form_text', 'placeholder' => 'Немного обо мне', 'size' => '30x5', 'required' => 'required')) }}
        {{ Form::submit('Зарегистрироваться', array('class' => 'form_btn')) }}
        {{ Form::close() }}
    </div>
@endsection