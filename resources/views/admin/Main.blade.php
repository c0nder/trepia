@extends('../Main')

@section('title')
    Админ-панель | Trepia
@endsection

@section('header')
    {{ Html::style('css/admin-panel.css') }}
@endsection

@section('content')
    <div class="page">
        <div class="windows left_col">
            <div class="window">
                <div class="w_title">Добавление пользователя</div>

                {{ Form::open(array('url' => 'admin/add/user')) }}

                @if(count($errors->getBag('register')->all()) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->getBag('register')->all() as $error)
                                <li>{{ $error  }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <p>
                    <span>E-mail пользователя:</span> <br />
                    {{ Form::email('users_email', null, array('class' => 'text_field', 'required' => 'required')) }}
                </p>

                <p>
                    <span>Пароль пользователя:</span> <br />
                    {{ Form::password('users_pass', array('class' => 'text_field', 'required' => 'required')) }}
                </p>

                <p>
                    <span>Повторите пароль:</span>
                    {{ Form::password('users_confirm_pass', array('class' => 'text_field', 'required' => 'required')) }}
                </p>

                <p>
                    <span>Ссылка на Youtube канал:</span> <br />
                    {{ Form::url('ytb_url', null, array('class' => 'text_field', 'required' => 'required')) }}
                </p>

                <p>
                    {{ Form::submit('Зарегистрировать аккаунт', array('class' => 'btn')) }}
                </p>

                {{ Form::close() }}
            </div>
        </div>

        <div class="windows right_col">
            <div class="window">
                <div class="w_title">Заявки на вывод денег</div>
            </div>
        </div>
    </div>

    <div class="page">
        <table class="admin_table">
            <tr>
                <th>id</th>
                <th>Логин</th>
                <th>E-mail</th>
                <th>Баланс</th>
            </tr>

            @foreach($users as $us)
                <tr>
                    <td>{{ $us->id }}</td>
                    <td>{{ $us->users_name }}</td>
                    <td>{{ $us->users_email }}</td>
                    <td>{{ $us->balance }}</td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection