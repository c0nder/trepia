<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<meta charset="UTF-8">
		<title>@yield('title')</title>

		{!! Html::style('css/main.css') !!}
		@yield('header')

		<meta name="yandex-verification" content="4bd9337c52fdc44b" />

		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
						(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
					m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-54152442-2', 'auto');
			ga('send', 'pageview');
		</script>
	</head>
	
	<body>
		<header>
			<div id="h_cont">
				<div id="h_logo">Trepia</div>
				<ul id="h_menu">
					<li><a href="http://trepia.ru/">Главная</a></li>
					<li><a href="https://vk.com/trepia">Мы в ВК</a></li>
					<li><a href="http://trepia.ru/auth/user/reg">Регистрация</a></li>
				</ul>
				@if(Auth::check())
					<div class="h_user">
						<a href="http://trepia.ru/dashboard">
						<div class="avatar">
							<img src="{{{ Auth::user()->avatar }}}" alt="Avatar">
						</div>

						<div class="nickname">
							{{ Auth::user()->users_name }}
						</div>
						</a>
					</div>
				@else
					<div class="login_or_reg">
						<a href="http://trepia.ru/auth/user/reg">Зарегистрироваться / Войти</a>
					</div>
				@endif
			</div>
		</header>

		@if(Session::get('header_error_message'))
			<div class="header_error_message header_alert">{{  Session::get('header_error_message') }}</div>
		@elseif(Session::get('header_success_message'))
			<div class="header_success_message header_alert">{{  Session::get('header_success_message') }}</div>
		@endif
		
		@yield('heroshot')
		
		<div class="wrapper">
			@yield('content')
		</div>

		<footer>&copy; Trepia 2016</footer>
	</body>
</html>