<div class="settings">
    <div class="title">Задать вопрос блогеру <b>{{ $user->users_name }}</b></div>

    <div class="content">
        {{ Form::open(array('url' => 'ask/' . $user->id)) }}

        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="row">
            <div class="set_title">Введите ваш вопрос:</div>
            {{ Form::textarea('question', '', array('class' => 'text_f', 'placeholder' => 'Ваш вопрос...', 'size' => '30x10', 'required' => 'required')) }}
        </div>

        <div class="row">
            {{ Form::submit('Задать вопрос') }}
        </div>

        {{ Form::close() }}
    </div>
</div>