@extends('Main')

@section('title')
	Главная | Trepia
@endsection

@section('heroshot')
	<div id="heroshot">
		<div id="heroshot_content">
			<h1>Давно мечтали пообщаться с любимым видео-блогером?</h1>

			<p>Давно мечтал пообщаться с любимым видео- блогером, тогда тебе именно сюда!
				Только у нас за символическую плату вы сможете пообщаться со звёздами YouTube. Все, что вам нужно сделать - это зарегистрироваться на нашем сайте, пополнить счет и выбрать собеседника.</p>

			<div id="heroshot_buttons">
				<div class="heroshot_btn"><a href="http://trepia.ru/auth/ytb/reg">Я - видео-блогер</a></div>
				<div class="heroshot_btn"><a href="http://trepia.ru/auth/user/reg">Я - хочу пообщаться</a><div>
			</div>
		</div>
	</div>
	</div>
	</div>
@endsection

@section('content')
	<div class="page">
		<h1 class="page_title">Зарегистрированные блогеры</h1>

		<div id="blogers">
			@foreach($youtubers as $ytb)
				<div class="bloger">
					<img src="{{{ $ytb->avatar }}}" alt="">
					<div><span class="nick">{{ $ytb->users_name }}</span></div>
					<div class="profile_subscribers"><span class="subscribers">{{ $ytb->subscribers/1000 }}k</span></div>
					<div class="profile_btn"><a href="{{{ $ytb->youtube_chanel }}}" class="profile ytb">YouTube</a></div>
					<div class="profile_btn"><a href="http://trepia.ru/dashboard/{{{ $ytb->id }}}" class="profile prf">Профиль</a></div>
				</div>
			@endforeach

		</div>
	</div>
@endsection