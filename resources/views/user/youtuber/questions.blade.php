@extends('user.youtuber.dashboard')

@section('right_column_content')
    <div class="settings">
        <div class="title">Вопросы ко мне</div>

        <div class="content">
            @if($questions->isEmpty())
                Вам не было задано вопросов :(
            @else
                @foreach($questions as $question)
                    <div class="question">
                        <div class="question_container">
                            <div class="avatar">
                                <img src="{{{ $question->user[0]->avatar }}}" alt="users' avatar">
                            </div>

                            <div class="q_content">
                                <div class="nick">
                                    {{ $question->user[0]->users_name }}
                                </div>

                                <div class="text">
                                    {{ $question->text }}
                                </div>

                                <div class="info">
                                    Пользователь: <a href="http://trepia.ru/dashboard/{{{ $question->user[0]->id }}}">{{ $question->user[0]->users_name }}</a>
                                </div>
                            </div>
                        </div>

                        <div class="answer">
                            <div class="title">Ваш ответ</div>

                            @if(!empty($question->answer))
                                <div class="avatar">
                                    <img src="{{{ $question->bloger[0]->avatar }}}" alt="bloger avatar">
                                </div>

                                <div class="ans_content">
                                    <div class="nick"> {{ $question->bloger[0]->users_name }} </div>

                                    <div class="text"> {{ $question->answer->text }} </div>
                                </div>
                            @else
                                {{ Form::open(['url' => 'dashboard/questions']) }}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                {{ Form::hidden('question_id', $question->id) }}
                                {{ Form::textarea('text', null, ['class' => 'text_field', 'placeholder' => 'Введите ваш ответ...']) }}
                                {{ Form::submit('Ответить') }}
                                {{ Form::close() }}
                            @endif
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
@endsection