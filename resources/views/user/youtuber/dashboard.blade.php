@extends('Main')

@section('title')
    Блогер {{ $user->users_name }}
@endsection

@section('header')
    {{ Html::style('css/dashboard.css') }}
@endsection

@section('content')
    <div class="page">
        <div class="user_profile left">
            <div id="avatar" class="window">
                <img src="{{{ $user->avatar }}}" alt="">

                <span class="username">{{ $user->users_name }}</span>

                <a href="http://trepia.ru/dashboard"><button class="avatar_btn">Мой профиль</button></a>
                <a href="http://trepia.ru/dashboard/questions"><button class="avatar_btn">Вопросы подписчиков</button></a>
                <a href="http://trepia.ru/dashboard/settings"><button class="avatar_btn">Настройки</button></a>
                <a href="http://trepia.ru/dashboard/logout"><button class="avatar_btn">Выйти</button></a>
            </div>

            <div class="user_information">
                <div class="title">Информация о пользователе</div>

                <div class="row">
                    <div class="name">Подписчики</div>

                    <div class="value">{{ $user->subscribers }}</div>
                </div>

                <div class="row">
                    <div class="name">Видео снято</div>

                    <div class="value">{{ $user->videos }}</div>
                </div>

                <div class="row">
                    <div class="name">Всего просмотров</div>

                    <div class="value">{{ $user->views }}</div>
                </div>

                <div class="row">
                    <div class="name">Баланс</div>

                    <div class="value">{{ $user->balance }} руб.</div>
                </div>
            </div>
        </div>

        <div class="user_action right">
            @yield('right_column_content')
        </div>
    </div>
@endsection