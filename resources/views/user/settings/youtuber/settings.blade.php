@extends('user.youtuber.dashboard')

@section('right_column_content')
    <div class="settings">
        <div class="title">Настройки</div>

        <div class="content">
            {{ Form::open(array('url' => 'dashboard/bloger/settings/save')) }}

            @if(count($errors->getBag('update_info')->all()) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->getBag('update_info')->all() as $error)
                            <li>{{ $error  }}</li>
                        @endforeach
                    </ul>
                </div>
            @elseif(Session::get('success'))
                <div class="alert alert-success">
                    Вы успешно изменили информацию
                </div>
            @endif

            {{ csrf_field() }}

            <div class="row">
                <div class="set_title">Изменить E-mail:</div>

                {{ Form::email('users_email', $user->users_email, array('class' => 'text_f', 'placeholder' => 'Email', 'required' => 'required')) }}
            </div>

            <div class="row">
                <div class="set_title">Изменить пароль:</div>

                {{ Form::password('old_password', array('class' => 'text_f', 'id' => 'old_password', 'placeholder' => 'Старый пароль')) }}

                {{ Form::password('new_password', array('class' => 'text_f', 'placeholder' => 'Новый пароль')) }}
            </div>

            <div class="row">
                <div class="set_title">Изменить цену на подписку:</div>

                {{ Form::text('sub_price', $user->sub_price, array('class' => 'text_f', 'required' => 'required')) }}

                <p class="description">* цена измеряется в рублях *</p>
            </div>

            <div class="row">
                {{ Form::submit('Сохранить изменения') }}
            </div>

            {{ Form::close() }}
        </div>
    </div>
@endsection