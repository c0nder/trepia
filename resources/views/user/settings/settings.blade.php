@extends('user.dashboard')

@section('right_column_content')
    <div class="settings">
        <div class="title">Настройки</div>

        <div class="content">
            {{ Form::open(array('url' => 'dashboard/settings/save')) }}

            @if(count($errors->getBag('update_info')->all()) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->getBag('update_info')->all() as $error)
                            <li>{{ $error  }}</li>
                        @endforeach
                    </ul>
                </div>
            @elseif(Session::get('success'))
                <div class="alert alert-success">
                    Вы успешно изменили информацию
                </div>
            @endif

            {{ csrf_field() }}

            <div class="row">
                <div class="set_title">Изменить Имя и Фамилию:</div>

                {{ Form::text('users_name', $user->users_name, array('class' => 'text_f', 'required' => 'required')) }}
            </div>

            <div class="row">
                <div class="set_title">Изменить E-mail:</div>

                {{ Form::email('users_email', $user->users_email, array('class' => 'text_f', 'placeholder' => 'Email', 'required' => 'required')) }}
            </div>

            <div class="row">
                <div class="set_title">Изменить пароль:</div>

                {{ Form::password('old_password', array('class' => 'text_f', 'id' => 'old_password', 'placeholder' => 'Старый пароль')) }}

                {{ Form::password('new_password', array('class' => 'text_f', 'placeholder' => 'Новый пароль')) }}
            </div>

            <div class="row">
                <div class="set_title">Изменить информация о себе:</div>
                {{ Form::textarea('users_inf', $user->users_inf, array('class' => 'text_f', 'placeholder' => 'Немного обо мне', 'size' => '30x5', 'required' => 'required')) }}
            </div>

            <div class="row">
                {{ Form::submit('Сохранить изменения') }}
            </div>

            {{ Form::close() }}
        </div>
    </div>
@endsection