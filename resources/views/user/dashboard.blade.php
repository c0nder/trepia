@extends('Main')

@section('title')
    Пользователь {{ $user->users_name }} | Trepia
@endsection

@section('header')
    {{ Html::style('css/dashboard.css') }}
@endsection

@section('content')
    <div class="page">
        <div class="user_profile left">
            <div id="avatar" class="window">
                <img src="{{{ $user->avatar }}}" alt="">

                <span class="username">{{ $user->users_name }}</span>

                <a href="http://trepia.ru/dashboard"><button class="avatar_btn">Мой профиль</button></a>
                <a href="http://trepia.ru/dashboard/questions"><button class="avatar_btn">Мои вопросы</button></a>

                {{ Form::open(['url' => 'dashboard/payment']) }}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <input type="submit" id="post_balance" value="Пополнить баланс: {{{ $user->balance }}} руб."></a>

                    <div id="balance_sum">
                        {{ Form::text('sum', '', ['id' => 'balance_field', 'placeholder' => 'Сумма пополнения баланса']) }}
                    </div>
                {{ Form::close() }}

                <a href="http://trepia.ru/dashboard/settings"><button class="avatar_btn">Настройки</button></a>
                <a href="http://trepia.ru/dashboard/logout"><button class="avatar_btn">Выйти</button></a>
            </div>

            <div class="user_information">
                <div class="title">Информация о пользователе</div>

                <div class="row">
                    <div class="name">Постов написано</div>

                    <div class="value">{{ count($posts) }}</div>
                </div>

                <div class="row">
                    <div class="name">Баланс</div>

                    <div class="value">{{ $user->balance }} руб.</div>
                </div>
            </div>
        </div>

        <div class="user_action right">
            @yield('right_column_content')
        </div>
    </div>
@endsection