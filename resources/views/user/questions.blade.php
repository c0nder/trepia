@extends('user.dashboard')

@section('right_column_content')
    <div class="settings">
        <div class="title">Мои вопросы</div>

        <div class="content">
            @if($questions->isEmpty())
                <h1>Вы пока не задали никому вопросов :(</h1>
            @else
                @foreach($questions as $question)
                    <div class="question">
                        <div class="question_container">
                            <div class="avatar">
                                <img src="{{{ $user->avatar }}}" alt="users' avatar">
                            </div>

                            <div class="q_content">
                                <div class="nick">
                                    {{ $user->users_name }}
                                </div>

                                <div class="text">
                                    {{ $question->text }}
                                </div>

                                <div class="info">
                                    Блогер: <a href="http://trepia.ru/dashboard/{{{ $question->bloger[0]->id }}}">{{ $question->bloger[0]->users_name }}</a>
                                </div>
                            </div>
                        </div>

                        <div class="answer">
                            <div class="title">Ответ от блогера</div>

                            @if(!empty($question->answer))
                                <div class="avatar">
                                    <img src="{{{ $question->bloger[0]->avatar }}}" alt="bloger avatar">
                                </div>

                                <div class="ans_content">
                                    <div class="nick"> {{ $question->bloger[0]->users_name }} </div>

                                    <div class="text"> {{ $question->answer->text }} </div>
                                </div>
                            @else
                                <div class="not_answered">На этот вопрос ответа пока нету :(</div>
                            @endif
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
@endsection