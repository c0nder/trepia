@extends($template)

@section('right_column_content')
    <div class="channel_info">
        <div class="title">Немного о пользователе</div>

        <div class="content">{{ $user->users_inf }}</div>
    </div>

    <div class="wall">
        <div class="post_form">
            {{ Form::open(array('url' => 'dashboard/post')) }}
            {{ Form::text('post_text', '', array('placeholder' => 'Что вы думаете?', 'id' => 'post_text', 'required' => 'required')) }}
            {{ Form::submit('Добавить пост') }}
            {{ Form::close() }}
        </div>

        @if(!empty($posts))
            @foreach($posts as $post)
                <div class="post">
                    <div class="post_content">
                        <div class="username">{{ $user->users_name }}</div>
                        <div class="message">{{ $post->message }}</div>
                        <div class="links">
                            <span class="date">{{ $post->created_at }} | </span>
                            <a href="dashboard/delete/post/{{{ $post->id }}}">Удалить</a>
                        </div>
                    </div>

                    <div class="avatar">
                        <img src="{{{ $user->avatar }}}" alt="">
                    </div>
                </div>
            @endforeach
        @else
            <div id="posts_null">
                На этой стене ничего не написано :(
            </div>
        @endif
    </div>
@endsection