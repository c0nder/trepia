@extends('Main')

@section('title')
    Пользователь {{ $user->users_name }}
@endsection

@section('header')
    {{ Html::style('css/dashboard.css') }}
@endsection

@section('content')
    <div class="page">
        <div class="user_profile left">
            <div id="avatar" class="window">
                <img src="{{{ $user->avatar }}}" alt="">

                <span class="username">{{ $user->users_name }}</span>

                @if(Auth::user()->users_type == 1)
                    <a href="http://trepia.ru/admin/loginUser/{{{ $user->id }}}"><button class="avatar_btn">Войти за этого пользователя</button></a>
                @endif
            </div>

            <div class="user_information">
                <div class="title">Информация о пользователе</div>

                <div class="row">
                    <div class="name">Постов написано</div>

                    <div class="value">{{ count($posts) }}</div>
                </div>
            </div>
        </div>

        <div class="user_action right">
            @include($include_temp, ['posts' => $posts, 'user' => $user])
        </div>
    </div>
@endsection