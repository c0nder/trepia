<div class="channel_info">
    @if($user->users_type == 2)
        <div class="title">Немного о канале</div>
    @else
        <div class="title">Немного о пользователе</div>
    @endif

    <div class="content">{{ $user->users_inf }}</div>
</div>

@if(Auth::id() != $user->id && $user->users_type == 2)
    <div class="channel_info">
        <div class="title">Что я получаю при подписке?</div>

        <div class="content">При подписке на блогера у вас появляется возможность пообщаться с ним. Так же в скором будет добавлена функция заказа «Сигн», так что не пропусти свой шанс!</div>
    </div>
@endif

<div class="wall">
    @if(!empty($posts))
        @foreach($posts as $post)
            <div class="post">
                <div class="post_content">
                    <div class="username">{{ $user->users_name }}</div>
                    <div class="message">{{ $post->message }}</div>
                    <div class="links">
                        <span class="date">{{ $post->created_at }}</span>
                    </div>
                </div>

                <div class="avatar">
                    <img src="{{{ $user->avatar }}}" alt="">
                </div>
            </div>
        @endforeach
    @else
        <div id="posts_null">
            На этой стене ничего не написано :(
        </div>
    @endif
</div>