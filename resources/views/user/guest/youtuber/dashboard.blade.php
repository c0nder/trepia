@extends('Main')

@section('title')
    Блогер {{ $user->users_name }}
@endsection

@section('header')
    {{ Html::style('css/dashboard.css') }}
@endsection

@section('content')
    <div class="page">
        <div class="user_profile left">
            <div id="avatar" class="window">
                <img src="{{{ $user->avatar }}}" alt="">

                <span class="username">{{ $user->users_name }}</span>

                @if($isSigned)
                    <a href="http://trepia.ru/ask/{{{ $user->id }}}"><button class="avatar_btn">Задать вопрос</button></a>
                    <a><button class="avatar_btn">Вы уже подписаны</button></a>
                @else
                    <a href="http://trepia.ru/subscribe/{{{ $user->id }}}"><button class="avatar_btn">Подписаться за <b>{{ $user->sub_price }} руб.</b></button></a>
                @endif
            </div>

            <div class="user_information">
                <div class="title">Информация о пользователе</div>

                <div class="row">
                    <div class="name">Подписчики</div>

                    <div class="value">{{ $user->subscribers }}</div>
                </div>

                <div class="row">
                    <div class="name">Видео снято</div>

                    <div class="value">{{ $user->videos }}</div>
                </div>

                <div class="row">
                    <div class="name">Всего просмотров</div>

                    <div class="value">{{ $user->views }}</div>
                </div>
            </div>
        </div>

        <div class="user_action right">
            @include($include_temp, ['posts' => $posts, 'user' => $user])
        </div>
    </div>
@endsection